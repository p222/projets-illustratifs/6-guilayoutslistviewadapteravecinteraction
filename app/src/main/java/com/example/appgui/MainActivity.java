package com.example.appgui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static int valeurNB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Grille linear avec poids
        setContentView(R.layout.linear);

        // Récupération de la liste
        ListView listeV = (ListView) findViewById(R.id.idListView);

        // Liste des valeurs affchées dans la listeView
        List<String> listeValeursDansLaListe = new ArrayList<>();

        // Création d'un adapter à partir de la liste
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listeValeursDansLaListe);

        // lie l'adapter à la listeView
        listeV.setAdapter(adapter);

        //---------------------------------------------------
        Button addButton = (Button)findViewById(R.id.idButtonAdd);
        Button deleteButton = (Button)findViewById(R.id.idButtonDelete);

        // Pour desactiver le bouton lorsqu'il n'y a pas de valeur à supprimer.. cf code bouton deleteButton
        deleteButton.setEnabled(false);

        // On va ajouter une valeur dans la liste à chaque fois que l'on appuiera sur le bouton +
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listeValeursDansLaListe.add("valeur "+(++valeurNB));
                adapter.notifyDataSetChanged();

                // Pour traiter le pb de la liste vide... Cf ci-dessous
                if (!deleteButton.isEnabled()){
                    deleteButton.setEnabled(true);
                }
            }
        });


        // On va enlever la première valeur dans la liste à chaque fois que l'on appuiera sur le bouton -
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // bof bof ....
                /*
                if (listeValeursDansLaListe.size()>0) {
                    listeValeursDansLaListe.remove(0);
                    adapter.notifyDataSetChanged();
                }
                */

                // Il vaudrait mieux désactiver le bouton s'il n'y a plus de valeur
                if (listeValeursDansLaListe.size()>0) {
                    listeValeursDansLaListe.remove(0);
                    adapter.notifyDataSetChanged();
                    if (listeValeursDansLaListe.size() == 0){
                        deleteButton.setEnabled(false);
                    }
                }

            }
        });
    }
}